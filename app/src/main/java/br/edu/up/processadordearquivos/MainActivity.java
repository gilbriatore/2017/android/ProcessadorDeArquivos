package br.edu.up.processadordearquivos;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

  private EditText txtInterno;
  private EditText txtExterno;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    txtInterno = (EditText) findViewById(R.id.txtInterno);
    txtExterno = (EditText) findViewById(R.id.txtExterno);
  }

  public void onClickGravarInterno(View view) {
    FileOutputStream fos = null;
    try {
      String texto = txtInterno.getText().toString();
      fos = openFileOutput("arquivo.txt", MODE_PRIVATE);
      fos.write(texto.getBytes());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (fos != null){
        try {
          fos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    txtInterno.setText("");
  }

  public void onClickCarregarInterno(View view) {
    FileInputStream fis = null;
    try{
      fis = openFileInput("arquivo.txt");
      byte[] buffer = new byte[fis.available()];
      fis.read(buffer);
      String texto = new String(buffer);
      txtInterno.setText(texto);
    } catch (Exception e) {
      e.printStackTrace();
    }  finally {
      if (fis != null){
        try {
          fis.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void onClickExcluirInterno(View view) {
    File caminho = getFilesDir();
    File arquivo = new File(caminho, "arquivo.txt");
    if (arquivo.exists()) {
      arquivo.delete();
    }
    txtInterno.setText("");
  }

  public void onClickGravarExterno(View view) {
    FileOutputStream fos = null;
    try {
      String texto = txtExterno.getText().toString();
      File sdCard = Environment.getExternalStorageDirectory();
      File arquivo = new File(sdCard, "arquivo.txt");
      fos = new FileOutputStream(arquivo);
      fos.write(texto.getBytes());
    } catch (Exception e){
      e.printStackTrace();
    } finally {
      if (fos != null){
        try {
          fos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    txtExterno.setText("");
  }

  public void onClickCarregarExterno(View view) {
    FileInputStream fis = null;
    try{
      File sdCard = Environment.getExternalStorageDirectory();
      File arquivo = new File(sdCard, "arquivo.txt");
      fis = new FileInputStream(arquivo);
      byte[] buffer = new byte[fis.available()];
      fis.read(buffer);
      String texto = new String(buffer);
      txtExterno.setText(texto);
    }catch (Exception e){
      e.printStackTrace();
    } finally {
      if (fis != null){
        try {
          fis.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void onClickExcluirExterno(View view) {
    File sdCard = Environment.getExternalStorageDirectory();
    File arquivo = new File(sdCard, "arquivo.txt");
    if (arquivo.exists()){
      arquivo.delete();
    }
    txtExterno.setText("");
  }
}
